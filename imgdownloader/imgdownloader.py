# -*- coding: utf-8 -*-
# @Author: Johan Olsson
# @Date:   2018-02-17 09:05:24
# @Last Modified by:   Johan Olsson
# @Last Modified time: 2018-02-18 09:30:51
"""
    imgdownloader
    =============
    This simple module takes a plain text file containing image URLs (one per
    line) and downloads them one by one to a definable directory.
    :license: MIT, see LICENSE for details.
"""

from __future__ import print_function
import os
import sys
import argparse
import urllib2
import urlparse
import hashlib
import colorlog


def main():
    """The method called when this module is invoked from the command line"""
    args = parse_command_line_arguments()
    download_from_file(args.infile, args.outdir)


def parse_command_line_arguments():
    """Parses and validates command line arguments"""
    parser = argparse.ArgumentParser(
        description='Download images from the internet.')
    parser.add_argument('infile',
                        help='Text file containing a list of image URLs.',
                        type=argparse.FileType('r'))
    parser.add_argument('outdir',
                        help='The output directory.',
                        nargs='?',
                        default=os.getcwd())
    return parser.parse_args()


def download(filename, output_directory=os.getcwd()):
    """
    Begins downloading images from URLs contained in the file with the given
    filename.
    """
    with open(filename, 'r') as file_containing_image_urls:
        download_from_file(file_containing_image_urls, output_directory)


def download_from_file(file_containing_image_urls,
                       output_directory=os.getcwd()):
    """Begins downloading images from URLs contained in the given file."""
    ensure_output_directory_exists(output_directory)

    list_of_image_urls = get_list_of_urls_from_file(file_containing_image_urls)
    for image_url in list_of_image_urls:
        download_image(image_url, output_directory)


def ensure_output_directory_exists(output_directory=os.getcwd()):
    """
    Check existance of the desired destination directory. If it doesn't
    exist it will be created.
    """
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)


def is_response_an_image(content_type):
    """
    Check the specified Content Type if it is of type image.
    Returns True if it is an image, False otherwise.
    """
    return content_type.startswith('image') is True


def get_list_of_urls_from_file(file_containing_image_urls):
    """
    Reads all lines of the specified file and filters out any line not
    resembling a URL.
    """
    potential_urls = [
        line.strip() for line in file_containing_image_urls.readlines()]
    return [url for url in potential_urls if url and url.startswith('http')]


def download_image(image_url, output_directory=os.getcwd()):
    """
    Downloads the content of the URL if request succeeds and content is of type
    image.
    """
    logger = colorlog.getLogger(__name__)
    logger.debug('Invoking request', extra={"URL": image_url})

    response = None
    try:
        response = urllib2.urlopen(image_url)
        content_type = response.info().get('content-type', '')
        if is_response_an_image(content_type):
            filename = get_filename_from_url(image_url, output_directory)
            with open(filename, 'w') as image_file:
                image_file.write(response.read())
            logger.info("Image was downloaded successfully.",
                        extra={"URL": image_url, "full_path": filename})
        else:
            logger.error("Content is not an image.", extra={"URL": image_url})
    except urllib2.HTTPError as err:
        logger.error('Request failed. Status code %s' % err.code,
                     extra={"URL": image_url})
    except urllib2.URLError as err:
        logger.error('Request failed. %s' % err.message,
                     extra={"URL": image_url})
    finally:
        if response:
            response.close()
            response = None


def get_filename_from_url(url, output_directory=os.getcwd()):
    """
    Tries to extract the filename from the specified URL. If no filename can be
    extracted the filename will default to the MD5 hash of the URL.
    """
    last_path_segment = os.path.basename(urlparse.urlparse(url).path)
    if not last_path_segment:
        last_path_segment = hashlib.md5(url).hexdigest()
    return os.path.join(output_directory, last_path_segment)


if __name__ == '__main__':
    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter(
        '%(asctime)s %(log_color)s%(levelname)-8s%(reset)s | '
        '%(log_color)s%(message)s\n'
        '%(reset)36s | %(log_color)s%(URL)s%(reset)s'))
    logger = colorlog.getLogger(__name__)
    logger.addHandler(handler)
    logger.setLevel('DEBUG')
    sys.exit(main())
