# -*- coding: utf-8 -*-
# @Author: Johan Olsson
# @Date:   2018-02-17 10:23:24
# @Last Modified by:   Johan Olsson
# @Last Modified time: 2018-02-17 14:07:00

from imgdownloader import get_filename_from_url

import unittest


class TestGetFilenameFromUrl(unittest.TestCase):

    def test_should_extract_image_filename(self):
        result = get_filename_from_url(
            'http://web.page/image.jpg', 'images/')
        self.assertEqual('images/image.jpg', result)

    def test_should_extract_image_filename_without_base_directory(self):
        result = get_filename_from_url(
            'http://web.page/image.jpg', '')
        self.assertEqual('image.jpg', result)

    def test_should_hash_url(self):
        result = get_filename_from_url(
            'http://web.page/image/', 'images/')
        self.assertEqual('images/574d7d253d8c1b93752960febf1614fd', result)

    def test_should_hash_url_without_base_directory(self):
        result = get_filename_from_url(
            'http://web.page/image/', '')
        self.assertEqual('574d7d253d8c1b93752960febf1614fd', result)
