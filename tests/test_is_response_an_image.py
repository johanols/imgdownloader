# -*- coding: utf-8 -*-
# @Author: Johan Olsson
# @Date:   2018-02-17 13:55:42
# @Last Modified by:   Johan Olsson
# @Last Modified time: 2018-02-17 13:59:13

from imgdownloader import is_response_an_image

import unittest


class TestIsResponseAnImage(unittest.TestCase):

    def test_text_plain_should_return_false(self):
        result = is_response_an_image('text/plain')
        self.assertFalse(result)

    def test_image_jpeg_should_return_true(self):
        result = is_response_an_image('image/jpeg')
        self.assertTrue(result)

    def test_empty_should_return_false(self):
        result = is_response_an_image('')
        self.assertFalse(result)
