# -*- coding: utf-8 -*-
# @Author: Johan Olsson
# @Date:   2018-02-17 14:05:04
# @Last Modified by:   Johan Olsson
# @Last Modified time: 2018-02-17 15:11:50

from imgdownloader import get_list_of_urls_from_file
from mock import Mock

import unittest


class TestGetListOfUrlsFromFile(unittest.TestCase):

    def setUp(self):
        self.mock_file = Mock()

    def test_should_extract_no_urls_from_empty_file(self):
        self.mock_file.readlines = Mock(return_value=[])
        result = get_list_of_urls_from_file(self.mock_file)
        self.mock_file.readlines.assert_called_once()
        self.assertEqual(result, [])

    def test_should_extract_all_urls_from_file(self):
        self.mock_file.readlines = Mock(
            return_value=['http://web.site/image.jpg'])
        result = get_list_of_urls_from_file(self.mock_file)
        self.mock_file.readlines.assert_called_once()
        self.assertEqual(result, ['http://web.site/image.jpg'])

    def test_should_not_extract_empty_url_from_file(self):
        self.mock_file.readlines = Mock(
            return_value=[''])
        result = get_list_of_urls_from_file(self.mock_file)
        self.mock_file.readlines.assert_called_once()
        self.assertEqual(result, [])

    def test_should_not_extract_unsupported_url_from_file(self):
        self.mock_file.readlines = Mock(
            return_value=['unsupportedscheme://web.site/image.jpg'])
        result = get_list_of_urls_from_file(self.mock_file)
        self.mock_file.readlines.assert_called_once()
        self.assertEqual(result, [])

    def test_should_trim_url_from_file(self):
        self.mock_file.readlines = Mock(
            return_value=['  http://web.site/image.jpg \t\n'])
        result = get_list_of_urls_from_file(self.mock_file)
        self.mock_file.readlines.assert_called_once()
        self.assertEqual(result, ['http://web.site/image.jpg'])
