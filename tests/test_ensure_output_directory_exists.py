# -*- coding: utf-8 -*-
# @Author: Johan Olsson
# @Date:   2018-02-17 16:03:13
# @Last Modified by:   Johan Olsson
# @Last Modified time: 2018-02-17 17:19:18

from imgdownloader import ensure_output_directory_exists
from mock import patch

import unittest


def foo(arg):
    return True


class TestEnsureOutputDirectoryExists(unittest.TestCase):

    @patch('os.makedirs')
    @patch('os.path.exists', side_effect=lambda arg: False)
    def test_should_create_missing_folder(self, mock_exists, mock_makedirs):
        ensure_output_directory_exists('download-folder')
        mock_makedirs.assert_called_once_with('download-folder')

    @patch('os.makedirs')
    @patch('os.path.exists', side_effect=lambda arg: True)
    def test_should_not_create_folder(self, mock_exists, mock_makedirs):
        ensure_output_directory_exists('download-folder')
        mock_makedirs.assert_not_called()
