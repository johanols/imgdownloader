# -*- coding: utf-8 -*-
# @Author: Johan Olsson
# @Date:   2018-02-17 17:01:42
# @Last Modified by:   Johan Olsson
# @Last Modified time: 2018-02-17 19:27:01

from imgdownloader import download_image
from mock import patch

import unittest
import urllib2


class TestDownloadImage(unittest.TestCase):

    @patch('__builtin__.open')
    @patch('urllib2.urlopen', side_effect=urllib2.HTTPError(
        None, 404, None, None, None))
    def test_http_error_exception_handling(self, mock_urlopen, mock_open):
        download_image('http://web.site/image.jpg')
        mock_urlopen.info.assert_not_called()
        mock_open.assert_not_called()

    @patch('__builtin__.open')
    @patch('urllib2.urlopen', side_effect=urllib2.URLError(None))
    def test_url_error_exception_handling(self, mock_urlopen, mock_open):
        download_image('http://web.site/image.jpg')
        mock_urlopen.info.assert_not_called()
        mock_open.assert_not_called()

    @patch('__builtin__.open')
    @patch('urllib2.urlopen')
    def test_should_not_download_text_content(self, mock_urlopen, mock_open):
        mock_response = mock_urlopen(any)
        mock_response.info.return_value.get.return_value = 'text/plain'
        download_image('http://web.site/image.jpg')
        mock_response.info.assert_called_once()
        mock_response.info.return_value.get.assert_called_once_with(
            'content-type', '')
        mock_open.assert_not_called()
        mock_response.close.assert_called_once()

    @patch('__builtin__.open')
    @patch('urllib2.urlopen')
    def test_should_download_image(self, mock_urlopen, mock_open):
        mock_response = mock_urlopen(any)
        mock_response.info.return_value.get.return_value = 'image/jpeg'
        download_image('http://web.site/image.jpg')
        mock_response.info.assert_called_once()
        mock_response.info.return_value.get.assert_called_once_with(
            'content-type', '')
        mock_open.assert_called_once()
        mock_response.close.assert_called_once()
