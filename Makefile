init:
	pip install -r requirements.txt

test:
	python -m unittest discover -b

coverage:
	coverage run -m unittest discover -b
	coverage report

doc:
	make -C docs

clean:
	rm -f .coverage
	rm -rf build dist *.egg-info
	rm -f ./docs/*.html