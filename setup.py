# -*- coding: utf-8 -*-
# @Author: Johan Olsson
# @Date:   2018-02-17 13:05:17
# @Last Modified by:   Johan Olsson
# @Last Modified time: 2018-02-18 09:51:26

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='imgdownloader',
    version='1.0.0',
    description='Downloading images listed in a plain text file.',
    long_description=readme,
    author='Johan Olsson',
    author_email='johan@jmvo.se',
    url='https://bitbucket.org/johanols/imgdownloader/',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
