imgdownloader
=============

This simple module takes a plain text file containing image URLs (one per line) and downloads them one by one to a definable directory.

Prerequisite
-----------

To install all dependencies needed execute the following command. Make sure you have [pip](https://pip.pypa.io/en/stable/installing/) installed.
```
make
```

Usage
-----

To use via the command line:
```
python imgdownloader/imgdownloader.py [-h] infile [outdir]
```

E.g.
```
python imgdownloader/imgdownloader.py samples/sample.txt images
```

To use it as a module:
```
import imgdownloader
imgdownloader.download('samples/sample.txt', 'images')
```

Tests
-----

To run the tests execute:
```
make test
```

To get a code coverage report in addition to the test report execute:
```
make coverage
```

Documentation
-------------

To create the documentation execute the following command. The documentation will then be available in the `docs` directory.
```
make doc
```

License
-------
```
MIT License

Copyright (c) 2018 Johan Olsson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```